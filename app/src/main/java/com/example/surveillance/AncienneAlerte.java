package com.example.surveillance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class AncienneAlerte extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ancienne_alerte);
    }

    //todo afficher la vidéo de l'alerte
    public void voirVideo(View V){
        Toast toast = Toast.makeText(getApplicationContext(), "La vidéo va s'afficher sous peu...", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo afficher les images de l'alerte
    public void voirImages(View v){
        Toast toast = Toast.makeText(getApplicationContext(), "Les images vont s'afficher sous peu...", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo stocker l'alerte qq part dans la bdd
    public void enregistrerAlerte(View v){
        Toast toast = Toast.makeText(getApplicationContext(), "Enregistrement effectué !", Toast.LENGTH_LONG);
        toast.show();
    }

    public void viewParams(View v) {
        Intent param = new Intent(this, Parametres.class);
        startActivity(param);
    }
}
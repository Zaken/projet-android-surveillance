package com.example.surveillance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //todo si identifiants corrects alors changement de vue + connexion
    public void connexion(View v) {
        Intent connect = new Intent(MainActivity.this, FirstConnexion.class);
        startActivity(connect);
    }

    //todo probablement rien de plus que ça
    public void changermdp(View v) {
        Intent change = new Intent(MainActivity.this, Changermdp.class);
        startActivity(change);
    }
}
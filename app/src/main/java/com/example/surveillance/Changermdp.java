package com.example.surveillance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Changermdp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changermdp);
    }

    public void changeMdp(View v) {
        /*lance une méthode qui met à jour les infos de l'utilisateur*/
        //puis appelle la méthode "rappeler plus tard" qui consiste simplement à lancer l'activité de l'accueil
        Intent connect = new Intent(this, Accueil.class);
        startActivity(connect);
    }

    //todo probablement rien de plus que ça
    public void ignore(View v) {
        Intent connect = new Intent(this, Accueil.class);
        startActivity(connect);
    }
}
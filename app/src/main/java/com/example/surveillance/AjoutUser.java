package com.example.surveillance;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AjoutUser extends AppCompatActivity {
    private EditText login;
    private EditText mdp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_user);
        login = findViewById(R.id.edit1);
    }

    //todo enregistrer dans la bdd user les id entrés par le superutilisateur
    public void addUser(View V){
        Toast toast = Toast.makeText(getApplicationContext(), "Il n'est pas encore possible d'ajouter un utilisateur.", Toast.LENGTH_LONG);
        toast.show();
    }
}
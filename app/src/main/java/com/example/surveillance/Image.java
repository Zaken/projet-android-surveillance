package com.example.surveillance;


import java.io.Serializable;
import java.util.Date;

public class Image implements Serializable {
    private String name;
    private String local;
    private String dir;
    private Date date;

    public Image(){

    }

    public Image(String name, String local, String dir, Date date) {
        this.name = name;
        this.local = local;
        this.dir = dir;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

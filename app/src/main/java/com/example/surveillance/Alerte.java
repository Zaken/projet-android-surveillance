package com.example.surveillance;


import java.io.Serializable;
import java.util.Date;

public class Alerte implements Serializable {
    private Date date;
    private String local;
    private String alerterUser;
    private String[] alertedUsers;
    private String alertVideo;
    private String alertImages;
    private String actionsTaken;
    private Boolean isRecent;
    private String status;

    public Alerte(){

    }

    public Alerte(Date date, String local, String alerterUser, String[] alertedUsers, String alertVideo, String alertImages, String actionsTaken, Boolean isRecent, String status) {
        this.date = date;
        this.local = local;
        this.alerterUser = alerterUser;
        this.alertedUsers = alertedUsers;
        this.alertVideo = alertVideo;
        this.alertImages = alertImages;
        this.actionsTaken = actionsTaken;
        this.isRecent = isRecent;
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getAlerterUser() {
        return alerterUser;
    }

    public void setAlerterUser(String alerterUser) {
        this.alerterUser = alerterUser;
    }

    public String[] getAlertedUsers() {
        return alertedUsers;
    }

    public void setAlertedUsers(String[] alertedUsers) {
        this.alertedUsers = alertedUsers;
    }

    public String getAlertVideo() {
        return alertVideo;
    }

    public void setAlertVideo(String alertVideo) {
        this.alertVideo = alertVideo;
    }

    public String getAlertImages() {
        return alertImages;
    }

    public void setAlertImages(String alertImages) {
        this.alertImages = alertImages;
    }

    public String getActionsTaken() {
        return actionsTaken;
    }

    public void setActionsTaken(String actionsTaken) {
        this.actionsTaken = actionsTaken;
    }

    public Boolean getRecent() {
        return isRecent;
    }

    public void setRecent(Boolean recent) {
        isRecent = recent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

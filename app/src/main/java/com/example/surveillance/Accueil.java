package com.example.surveillance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Accueil extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
    }

    //todo il faudra créer dynamiquement les différentes cardview

    public void viewLocal(View v) {
        Intent local = new Intent(this, Local.class);
        startActivity(local);
    }

    //todo rajouter une vue pour la visualisation des vidéos d'un local
    public void modeVideo(View v) {
        Intent local = new Intent(this, Local.class);
        startActivity(local);
    }

    public void modeImage(View v) {
        Intent local = new Intent(this, ViewImage.class);
        startActivity(local);
    }

    public void histoAlerte(View v) {
        Intent local = new Intent(this, ViewAlertes.class);
        startActivity(local);
    }

    //todo rajouter une vue pour l'historique des visualisations (quasi identique aux alertes)
    public void histoVisu(View v) {
        Intent local = new Intent(this, Local.class);
        startActivity(local);
    }

    //todo rajouter une vue pour l'historique des enregistrements (quasi identique aux alertes)
    public void histoRecord(View v) {
        Intent local = new Intent(this, Local.class);
        startActivity(local);
    }

    public void viewParams(View v) {
        Intent param = new Intent(this, Parametres.class);
        startActivity(param);
    }
}
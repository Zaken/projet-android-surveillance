package com.example.surveillance;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class AlerteEnCours extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerte_en_cours);
    }

    //todo display une vidéo au clic
    public void afficherVideo(View V){
        Toast toast = Toast.makeText(getApplicationContext(), "La vidéo va s'afficher sous peu...", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo display une (des?) images au clic
    public void afficherImage(View V){
        Toast toast = Toast.makeText(getApplicationContext(), "Les images vont s'afficher sous peu", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo probablement rien de plus qu'un toast pour celle-ci
    public void callSecu(View V){
        Toast toast = Toast.makeText(getApplicationContext(), "La sécurité a été appelée.", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo créer un petit menu avec 3 systèmes au pif sur lesquels cliquer (genre porte, fenêtres, lumière). enregistrer les actions choisies pour l'historique
    public void activateSystem(View V){
        Toast toast = Toast.makeText(getApplicationContext(), "Le menu des systèmes va s'afficher...", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo probablement rien de plus qu'un toast pour celle-ci
    public void launchAlarm(View V){
        Toast toast = Toast.makeText(getApplicationContext(), "L'alarme a été lancée !'", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo simple retour en arrière
    public void skip(View V){

    }
}
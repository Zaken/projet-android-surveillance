package com.example.surveillance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import java.io.Serializable;

public class Local extends AppCompatActivity implements Serializable {

    private String name;
    private String image;
    private String liveImages;
    private String liveVideo;
    private String securityPhone;
    private String currentUser;
    private String owner;
    private String adresse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local);
    }

    //rajouter une vue pour la visualisation des vidéos d'un local
    public void modeVideo(View v) {
        Intent local = new Intent(this, Local.class);
        startActivity(local);
    }

    public void modeImage(View v) {
        Intent local = new Intent(this, ViewImage.class);
        startActivity(local);
    }

    public void histoAlerte(View v) {
        Intent local = new Intent(this, ViewAlertes.class);
        startActivity(local);
    }

    //rajouter une vue pour l'historique des visualisations (quasi identique aux alertes)
    public void histoVisu(View v) {
        Intent local = new Intent(this, Local.class);
        startActivity(local);
    }

    //rajouter une vue pour l'historique des enregistrements (quasi identique aux alertes)
    public void histoRecord(View v) {
        Intent local = new Intent(this, Local.class);
        startActivity(local);
    }

    public void viewParams(View v) {
        Intent param = new Intent(this, Parametres.class);
        startActivity(param);
    }

    public Local(){
    }

    public Local(String name, String image, String liveImages, String liveVideo, String securityPhone, String currentUser, String owner, String adresse) {
        this.name = name;
        this.image = image;
        this.liveImages = liveImages;
        this.liveVideo = liveVideo;
        this.securityPhone = securityPhone;
        this.currentUser = currentUser;
        this.owner = owner;
        this.adresse = adresse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLiveImages() {
        return liveImages;
    }

    public void setLiveImages(String liveImages) {
        this.liveImages = liveImages;
    }

    public String getLiveVideo() {
        return liveVideo;
    }

    public void setLiveVideo(String liveVideo) {
        this.liveVideo = liveVideo;
    }

    public String getSecurityPhone() {
        return securityPhone;
    }

    public void setSecurityPhone(String securityPhone) {
        this.securityPhone = securityPhone;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void voirAlertes(View v) {
        Intent alertes = new Intent(this, ViewAlertes.class);
        startActivity(alertes);
    }
}
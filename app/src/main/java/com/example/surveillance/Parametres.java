package com.example.surveillance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Parametres extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametres);
    }

    //todo afficher un petit menu avec différents modes à choisir
    public void changeAlertes(View V){
        Toast toast = Toast.makeText(getApplicationContext(), "Mode d'alerte changé.", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo afficher un menu qui permettra d'entrer les différents paramètres du local (images, nom, etc.)
    public void creerLocal(View v) {
        Toast toast = Toast.makeText(getApplicationContext(), "Local créé. Où? Mystère...", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo afficher un menu qui permettra d'entrer les nouveaux identifiants
    public void changeIds(View v) {
        Toast toast = Toast.makeText(getApplicationContext(), "Vous ne pouvez pas encore changer vos identifiants.", Toast.LENGTH_LONG);
        toast.show();
    }

    public void generateAccounts(View v) {
        Intent connect = new Intent(this, AjoutUser.class);
        startActivity(connect);
    }

    //todo ouvrira simplement l'activité de l'historique des visualisations
    public void displayHisto(View v) {
        Toast toast = Toast.makeText(getApplicationContext(), "Vous ne pouvez pas encore consulter cet historique.", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo déconnecter i guess
    public void unlog(View v) {
        Toast toast = Toast.makeText(getApplicationContext(), "Déconnexion impossible. Vous êtes piégé ici pour l'éternité.", Toast.LENGTH_LONG);
        toast.show();
    }
}
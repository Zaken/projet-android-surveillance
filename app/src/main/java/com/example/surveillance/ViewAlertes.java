package com.example.surveillance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ViewAlertes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_alertes);
    }

    //todo ajouter dynamiquement les informations sur les différents champs des alertes

    public void voirAncienneAlerte(View v) {
        Intent old = new Intent(this, AncienneAlerte.class);
        startActivity(old);
    }

    public void voirAlerteEnCours(View v) {
        Intent cur = new Intent(this, AlerteEnCours.class);
        startActivity(cur);
    }

    public void viewParams(View v) {
        Intent param = new Intent(this, Parametres.class);
        startActivity(param);
    }
}
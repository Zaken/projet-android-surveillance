package com.example.surveillance;


import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private String login;
    private String password;
    private Date lastChange;
    private Boolean isSuperieur;
    private Boolean haveToChange;
    private String userSuperieur;

    public User(){

    }

    public User( String login, String password, Date lastChange, Boolean isSuperieur, Boolean haveToChange, String userSuperieur){
        this.login = login;
        this.password = password;
        this.haveToChange = haveToChange;
        this.isSuperieur = isSuperieur;
        this.lastChange = lastChange;
        this.userSuperieur = userSuperieur;
    }

    public void setLogin(String login){
        this.login = login;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public void setIsSuperieur(Boolean isSuperieur){
        this.isSuperieur = isSuperieur;
    }
    public void setLastChange(Date lastChange){
        this.lastChange = lastChange;
    }

    public void setHaveToChange(Boolean haveToChange) {
        this.haveToChange = haveToChange;
    }

    public void setUserSuperieur(String userSuperieur) {
        this.userSuperieur = userSuperieur;
    }

    public Boolean getHaveToChange() {
        return haveToChange;
    }

    public Boolean getSuperieur() {
        return isSuperieur;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Date getLastChange() {
        return lastChange;
    }

    public String getUserSuperieur() {
        return userSuperieur;
    }
}
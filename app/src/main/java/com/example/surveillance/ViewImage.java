package com.example.surveillance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ViewImage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewimage);
    }

    //todo enregistre les infos de l'alerte qq part dans la bdd
    public void enregistration(View v){
        Toast toast = Toast.makeText(getApplicationContext(), "Enregistrement effectué !", Toast.LENGTH_LONG);
        toast.show();
    }

    //todo probablement rien de plus qu'un toast
    public void alertage(View v){
        Toast toast = Toast.makeText(getApplicationContext(), "Alerte lancée", Toast.LENGTH_LONG);
        toast.show();
    }

    public void viewParams(View v) {
        Intent param = new Intent(this, Parametres.class);
        startActivity(param);
    }
}